const fs = require("fs");

function fileOperations(fileName) {
  fs.readFile(fileName, "utf-8", (error, content) => {
    if (error) {
      console.error(error);
    } else {
      console.log("file read successfully");
      fs.writeFile("upperCaseFile.txt", content.toUpperCase(), (error) => {
        if (error) {
          console.error(error);
        } else {
          console.log("Done writing file to uppercase");
          fs.appendFile("filenames.txt", "upperCaseFile.txt\n", (error) => {
            if (error) {
              console.error(error);
            } else {
              fs.readFile("upperCaseFile.txt", "utf-8", (error) => {
                if (error) {
                  console.error(error);
                } else {
                  console.log("uppercase file read successfully");
                  const lowerCase = content.toString().toLowerCase().split(". ").join(".\n");
                  fs.writeFile("lowerCase.txt", lowerCase, (error) => {
                    if (error) {
                      console.error(error);
                    } else {
                      console.log("done writing to lowerCase");
                      fs.appendFile("filenames.txt","lowerCase.txt\n",
                        (error) => {
                          if (error) {
                            console.error(error);
                          } else {
                            console.log("done writing to filename to filenames.txt");
                            fs.readFile("lowerCase.txt", "utf-8", (error) => {
                              if (error) {
                                console.error(error);
                              } else {
                                console.log("done reading lowerCase file");
                                let sort = content.toString().split("\n").sort().slice(3);
                                fs.writeFile("sortFile.txt",sort.join("\n").toString(),
                                  (error) => {
                                    if (error) {
                                      console.error(error);
                                    } else {
                                      fs.appendFile("filenames.txt","sortFile.txt\n",
                                        (error) => {
                                          if (error) {
                                            console.error(error);
                                          } else {
                                            console.log("done writing to file");
                                            fs.readFile("filenames.txt","utf-8",
                                              (error) => {
                                                if (error) {
                                                  console.error(error);
                                                } else {
                                                  console.log("done reading files");
                                                  fs.unlink("upperCaseFile.txt",
                                                    (error) => {
                                                      if (error) {
                                                        console.error(error);
                                                      } else {
                                                        fs.unlink("lowerCase.txt",
                                                          (error) => {
                                                            if (error) {
                                                              console.error(error);
                                                            } else {
                                                              fs.unlink("sortFile.txt",
                                                                (error) => {
                                                                  if (error) {
                                                                    console.error(error);
                                                                  } else {
                                                                    console.log("all files deleted successfully");
                                                                  }
                                                                }
                                                              );
                                                            }
                                                          }
                                                        );
                                                      }
                                                    }
                                                  );
                                                }
                                              }
                                            );
                                          }
                                        }
                                      );
                                    }
                                  }
                                );
                              }
                            });
                          }
                        }
                      );
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}

module.exports = fileOperations;
