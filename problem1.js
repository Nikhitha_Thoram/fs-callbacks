const fs = require("fs");

function createAndDeleteFiles(dirName) {
  let files = 0;
  let randomNumber = Math.round(Math.random() * 10);

  fs.mkdir(dirName, (error) => {
    if (error) {
      console.error(error);
    } else {
      console.log("directory created successfully");
      for (let index = 1; index <= randomNumber; index++) {
        fs.writeFile(`${dirName}/file_${index}.json`,JSON.stringify({ a: 10, b: 20 }),(error) => {
            if (error) {
              console.error(error);
            } else {
              files += 1;
              console.log(`created file_${index}.json successfully`);
              if (files === randomNumber) {
                console.log("started deleting files");
                for (let index = 1; index <=randomNumber; index++) {
                  fs.unlink(`${dirName}/file_${index}.json`, (error) => {
                    if (error) {
                      console.error(error);
                    } else {
                      console.log(`file_${index}.json was deleted successfully`);
                    }
                  });
                }
              }
            }
          }
        );
      }
    }
  });
}

module.exports = createAndDeleteFiles;

